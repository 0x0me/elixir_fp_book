defmodule Chap07 do
  # ListsAndRecursion-0
  def sum(xs), do: _sum(xs)

  defp _sum([]), do: 0
  defp _sum([head | tail]) do
    head+_sum(tail)
  end

  # ListsAndRecursion-1
  def mapsum(xs, func), do: _mapsum(xs,func)

  defp _mapsum([], _), do: 0
  defp _mapsum([head | tail], func) do
    func.(head)+_mapsum(tail, func)
  end

  # ListsAndRecursion-2
  def max([head | tail]), do: _max([head | tail], head)

  ## base case return accumulator as max
  defp _max([], mv), do: mv
  ## choose head as max if it is greater then our current max
  defp _max([head | tail], mv) when head > mv do
    _max(tail, head)
  end
  ## keep current max as total max
  defp _max([_ | tail], mv) do
    _max(tail, mv)
  end

  # ListsAndRecursion-3
  def caesar(xs,n), do: _caesar(xs,n)


  defp _caesar([], _), do: ''
  defp _caesar([head | tail], n) when (head+n) <= ?z  do
    [head+n | _caesar(tail, n) ]
  end

  defp _caesar([head | tail], n) when (head+n) > ?z do
    [?a+rem(head+n,?z)-1 | _caesar(tail, n)]
  end

  # ListsAndRecursion-4
  def span(xs, min, max), do: _span(xs,min,max)

  defp _span([], _, _), do: []
  defp _span([head | tail],min,max) when head in min..max, do: [head | _span(tail,min,max)]
  defp _span([head | tail],min,max), do: _span(tail,min,max)
end
