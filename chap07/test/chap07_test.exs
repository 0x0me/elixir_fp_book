defmodule Chap07Test do
  use ExUnit.Case

  test "ListsAndRecursion-0" do
    assert Chap07.sum([]) == 0
    assert Chap07.sum([1]) == 1
    assert Chap07.sum([1,2,3,4,5]) == 15
    assert Chap07.sum([1,2,3,4,5,6,7,8,9,10]) == 55
  end

  test "ListsAndRecursion-1" do
     assert Chap07.mapsum([1, 2, 3], &(&1 * &1)) == 14
  end

  test "ListsAndRecursion-2" do
    assert Chap07.max([1, 2, 3]) == 3
    assert Enum.shuffle([1,2,3,4,5,6,7,8,9,10]) |> Chap07.max() == 10
    assert Enum.shuffle([100,-100,1,2,3,4,5,6,7,8,9,10]) |> Chap07.max() == 100
  end

  test "ListsAndRecursion-3" do
    assert Chap07.caesar('ryvkve',13) == 'elixir'
  end

  test "ListsAndRecursion-4" do
    assert Chap07.span([100,-100,1,2,3,4,5,6,7,8,9,10],3,7) == [3,4,5,6,7]
  end
end
