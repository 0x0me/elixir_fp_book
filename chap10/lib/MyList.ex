defmodule MyList do

  def flatten(xs), do: _flatten(xs,[])

  defp _flatten([], acc), do: acc
  defp _flatten(e,acc) when not is_list(e), do: [e | acc]
  defp _flatten([head | tail],acc), do: _flatten(head,_flatten(tail,acc))

end
