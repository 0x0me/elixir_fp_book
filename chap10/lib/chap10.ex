defmodule Chap10 do
  ##############################################################################
  ## ListsAndRecursion-5 - all?
  ###
  def all?(xs, predicate), do: _all?(xs,predicate)

  defp _all?([],_), do: true
  defp _all?([head | tail], predicate) do
    predicate.(head) and _all?(tail, predicate)
  end
  ##############################################################################

  ##############################################################################
  #ListsAndRecursion-5 - each
  ###
  def each(xs, fun), do: _each(xs,fun)

  defp _each([],_), do: []
  defp _each([head | tail], fun) do
    fun.(head)

    [head | _each(tail,fun)]
  end
  ##############################################################################


  ##############################################################################
  #ListsAndRecursion-5
  ###
  def filter(xs, predicate), do: _filter(xs,predicate)

  defp _filter([],_), do: []
  defp _filter([head | tail], predicate) do
    if predicate.(head) do
      [head | _filter(tail, predicate)]
    else
      _filter(tail,predicate)
    end
  end
  ##############################################################################

  ##############################################################################
  #ListsAndRecursion-5 - split
  ###

  @doc """
  Mimicks Enum.split behaviour
  """

  @spec split(list(),number()) :: tuple()
  def split(xs, index), do: _split(xs,index,[])

  # base case empty list
  defp _split([],_,acc), do: {acc, []}

  # split up while lower than the index
  defp _split([head|tail], index, acc) when length(acc) < index do
    _split(tail, index, acc ++ [head])
  end

  # return tuple if greater then or equal to index
  defp _split(xs, index,acc) when length(acc) >= index do
    {acc, xs}
  end
  ##############################################################################

  #############################################################################
  #ListsAndRecursion-5 - take
  ###

  @doc """
  Mimicks Enum.take behaviour
  """
  def take(xs,num), do: _take(xs,num)

  defp _take([head|tail], num) when num > 0 do
    [head | _take(tail, num-1)]
  end

  defp _take([],_), do: []
  defp _take(_,0), do: []

  #############################################################################
  #ListsAndRecursion-7 - span helper
  ###

  @doc """
  get primes up to certain value
  """
  def primes(n) when n > 1 do
    for x <- 2..n, prime?(x), do: x
  end

  def prime?(2), do: true

  def prime?(x) do
    Enum.all?(2..(x-1), &(rem(x, &1) !== 0))
  end

  #############################################################################
  #ListsAndRecursion-8 - span helper
  ###

  @doc """
  get primes up to certain value
  """
  def tax(orders,rates) do
    for order <- orders, do: Dict.put(order,:total_amount,total(order[:net_amount], rates[order[:ship_to]]))
  end

  defp total(net_amount, rate) when is_number(rate), do: net_amount + (net_amount * rate)
  defp total(net_amount,_), do: net_amount



end
