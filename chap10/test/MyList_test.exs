defmodule MyListTest do
  use ExUnit.Case

  test "ListAndRecursion-6" do
    assert MyList.flatten([ 1, [ 2, 3, [4] ], 5, [[[6]]]]) == [1,2,3,4,5,6]

    list = [ 1, [ 2, 3, [4] ], 5, [[[6]]]]
    assert MyList.flatten(list) == List.flatten(list)
  end
end
