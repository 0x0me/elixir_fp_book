defmodule Chap10Test do
  use ExUnit.Case

  defp generate(boundary) do
    :random.seed(:erlang.now())
    for _x <- 1..div(boundary,4), do: :random.uniform(boundary)
  end

  test "ListAndRecursion-5 all?" do
    assert Chap10.all?([], &(&1 > 0)) == Enum.all?([], &(&1 > 0))

    list = [1,2,3,4,5,6,7,8,9,10]
    assert Chap10.all?(list, &(&1>0)) == Enum.all?(list, &(&1 > 0))


    boundary = 100
    l = generate(boundary)
    assert l |> Chap10.all?( &(&1 >= div(2*boundary,3) ) ) == l |> Enum.all?( &(&1 >= div(2*boundary,3)) )
  end


  test "ListAndRecursion-5 each" do
    l = generate(100)
    assert l |> Chap10.each( &(IO.inspect(&1*2))) === l
  end


  test "ListAndRecursion-5 filter" do
    assert Chap10.filter([], &(&1 > 0)) == Enum.filter([], &(&1 > 0))

    list = [1,2,3,4,5,6,7,8,9,10]
    assert Chap10.filter(list, &(&1>4)) == Enum.filter(list, &(&1 > 4))

    l = generate(100)
    assert l |> Chap10.filter( &(&1 <= 50) )  == l |> Enum.filter( &(&1 <= 50) )
  end

  test "ListAndRecursion-5 split" do
    assert Chap10.split([], 1) == Enum.split([], 1)

    list = [1,2,3,4,5,6,7,8,9,10]
    assert Chap10.split(list, 4) == Enum.split(list, 4)

    l = generate(100)
    assert l |> Chap10.split(25) == l |> Enum.split(25)

    l2 = generate(100)
    sp = div( length(l2), 2 )
    assert l2 |> Chap10.split(sp) == l2 |> Enum.split(sp)

  end


  test "ListAndRecursion-5 take" do
    assert Chap10.take([], 1) == Enum.take([], 1)

    list = [1,2,3,4,5,6,7,8,9,10]
    assert Chap10.take(list, 4) == Enum.take(list, 4)

    l = generate(100)
    assert l |> Chap10.take(25) == l |> Enum.take(25)

    l2 = generate(100)
    sp = div( length(l2), 2 )
    assert l2 |> Chap10.take(sp) == l2 |> Enum.take(sp)

  end

  test "ListAndRecursion-7" do
    assert Chap10.primes(10) === [2,3,5,7]
    assert Chap10.primes(40) === [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]
  end

  test "ListAndRecursion-8" do
    tax_rates = [ NC: 0.075, TX: 0.08 ]
    orders = [
        [ id: 123, ship_to: :NC, net_amount: 100.00 ],
        [ id: 124, ship_to: :OK, net_amount: 35.50 ],
        [ id: 125, ship_to: :TX, net_amount: 24.00 ],
        [ id: 126, ship_to: :TX, net_amount: 44.80 ],
        [ id: 128, ship_to: :NC, net_amount: 25.00 ],
        [ id: 127, ship_to: :MA, net_amount: 10.00 ],
        [ id: 129, ship_to: :CA, net_amount: 102.00 ],
        [ id: 120, ship_to: :NC, net_amount: 50.00 ]
      ]

    IO.inspect(Chap10.tax(orders, tax_rates))
  end

end
