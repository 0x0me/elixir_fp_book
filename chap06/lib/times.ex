defmodule Times do
  # given
  def double(n), do: n * 2

  # exercise p. 45
  # ModulesAndFunctions-1
  def triple(n), do: n *3

  # exercise p. 45
  # ModulesAndFunctions-3
  def quadruple(n), do: double(double(n))
end
