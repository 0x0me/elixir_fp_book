defmodule Chop do
  def guess(actual, range = a..b) do
    guess = div(a+b, 2)
    IO.puts "I guess it's #{guess}?"
    try_guess(actual, guess, range)
  end

  def try_guess(actual, actual, _), do: IO.puts "Right, it's #{actual} - well done!"

  def try_guess(actual, guess,  _low..high) when guess < actual, do: guess(actual, guess+1..high)

  def try_guess(actual, guess,  low.._high) when guess > actual, do: guess(actual, low..guess-1)
end
