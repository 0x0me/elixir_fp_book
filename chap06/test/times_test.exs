defmodule TimesTest do
  use ExUnit.Case

  test "ModulesAndFunctions-1" do
    assert Times.triple(0) == 0
    assert Times.triple(1) == 3
    assert Times.triple(2) == 6
  end

  test "ModulesAndFunctions-3" do
    assert Times.quadruple(2) == Times.double(2) |> Times.double
    assert Times.quadruple(4) == Times.double(4) |> Times.double
    assert Times.quadruple(0) == 0
  end
end
