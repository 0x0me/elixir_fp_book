defmodule Chap06Test do
  use ExUnit.Case

  test "ModulesAndFunctions-4" do
    assert Chap06.sum(0) == 0
    assert Chap06.sum(1) == 1
    assert Chap06.sum(10) == 55
  end

  test "ModulesAndFunctions-5" do
    assert Chap06.gcd(0,1) == 1
    assert Chap06.gcd(2,1) == 1
    assert Chap06.gcd(1,2) == 1
    assert Chap06.gcd(100,9) == 1
    assert Chap06.gcd(121,11) == 11
  end

  test "ModulesAndFunctions-7" do
    # convert float to string
    x = :io.format("it is ~4.2f~n",[:math.pi()])
    assert x == :ok

    # read environment variable
    assert System.get_env("ELIXIR_HOME") == "/opt/elixir/current/"

    # extension of last path compoent
    assert Path.extname("./test/chap06_test.exs") == ".exs"

    # current working directory
    System.cwd() |> IO.inspect

    { working_dir, exit_status } = System.cmd("pwd",[])
    assert String.strip(working_dir) == System.cwd()
    assert exit_status == 0

  end
end
