defmodule ChopTest do
  use ExUnit.Case

  test "ModulesAndFunctions-6" do
    assert Chop.guess(273, 1..1000) == :ok
  end
end
