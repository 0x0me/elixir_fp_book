defmodule Chap05Test do
  use ExUnit.Case

  # functions and pattern matching
  # exercise p. 35
  test "Functions-1" do
    list_concat = fn a , b -> a ++ b end
    assert list_concat.([:a, :b], [:c, :d]) == [:a, :b, :c, :d]

    sum = fn a, b, c -> a+b+c end
    assert sum.(1,2,3) == 6

    pair_tuple_to_list = fn a ->
      {first, second} = a
      [first, second]
    end
    assert pair_tuple_to_list.( {1234, 5678}) == [1234, 5678]

  end


  # one function with multiple bodies
  # exercise p. 37
  test "Functions-2" do
    fizzbuzz = fn
      {0, 0, _} -> "FizzBuzz"
      {0, _ ,_} -> "Fizz"
      {_, 0, _} -> "Buzz"
      {_, _, a} -> a
    end

    assert fizzbuzz.({0,1,2}) == "Fizz"
    assert fizzbuzz.({1,0,2}) == "Buzz"
    assert fizzbuzz.({0,0,0}) == "FizzBuzz"
    assert fizzbuzz.({1,2,3}) == 3
  end

  # one function with multiple bodies
  # exercise p.37
  test "Functions-3" do
    fizzbuzz = fn
      {0, 0, _} -> "FizzBuzz"
      {0, _ ,_} -> "Fizz"
      {_, 0, _} -> "Buzz"
      {_, _, a} -> a
    end

    f = fn n -> fizzbuzz.( {rem(n,3), rem(n,5), n}) end

    assert f.(10) == "Buzz"
    assert f.(11) == 11
    assert f.(12) == "Fizz"
    assert f.(13) == 13
    assert f.(14) == 14
    assert f.(15) == "FizzBuzz"
    assert f.(16) == 16
  end

  # functions returning functions
  #exercise p. 39
  test "Functions-4" do
    prefix = fn str1 ->
              fn str2 -> str1<>" "<>str2 end
            end

    mrs = prefix.("Mrs")
    assert mrs.("Smith") == "Mrs Smith"

    assert prefix.("Elixir").("Rocks") == "Elixir Rocks"
  end

  # passing functions and the & notation
  # exercise p. 42
  test "Functions-5" do
    assert Enum.map [1,2,3,4], &(&1+2) == Enum.map [1,2,3,4], fn x -> x+2 end
    Enum.map [1,2,3,4], &IO.inspect/1
  end
end
