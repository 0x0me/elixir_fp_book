defmodule Chap11 do
  @doc """
  Check if a given single-quoted string contains printable characters (inclusive
  between space and tilde) only
  """
  def printable?(xs), do: Enum.all?(xs, &_printable?(&1) )

  defp _printable?(ch) when ch in ?\s..?~,  do: true
  defp _printable?(ch), do: false


  def anagram(word1, word2) do
    Enum.sort(word1) === Enum.sort(word2)
  end


  @doc """
  Calculates the result of simple char list expressions.
  ```
  '123 + 27', '123+456/111'
  ```
  """
  def calculate(str) do
    _calculate(str,[])
  end

  @doc """
  Parse numbers
  """
  defp _calculate([ch|str], acc) when ch in '0123456789', do: _calculate(str, [ch|acc])

  @doc """
  Addition
  """
  defp _calculate([?+|str], acc), do: _to_integer(acc) + _calculate(str,[])
  @doc """
  Subtraction
  """
  defp _calculate([?-|str], acc), do: _to_integer(acc) - _calculate(str,[])
  @doc """
  Multiplication
  """
  defp _calculate([?*|str], acc), do: _to_integer(acc) * _calculate(str,[])
  @doc """
  Division
  """
  defp _calculate([?/|str], acc), do: _to_integer(acc) / _calculate(str,[])
  @doc """
  Ignore unmatched characters
  """
  defp _calculate([ch|str], acc), do: _calculate(str, acc)
  @doc """
  Base case recursion
  """
  defp _calculate([], acc), do: _to_integer(acc)

  @doc """
  Helper function to convert the reversed accumulator into the correct integer
  """
  defp _to_integer(rstr)do
    rstr
    |> List.to_string
    |> String.reverse
    |> String.to_integer
  end
end
