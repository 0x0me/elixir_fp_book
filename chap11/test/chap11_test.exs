defmodule Chap11Test do
  use ExUnit.Case

  import Chap11

  test "tringsAndBinaries-1" do
    assert printable?('a') === true
    assert printable?('not all printäble') === false
    assert printable?('all printable characters!') === true
  end

  test "tringsAndBinaries-2: parse" do

    assert calculate('12345') === 12345
    assert calculate('a1b2c3d4e5') === 12345
    assert calculate('123+456/111') === 127.10810810810811
    assert calculate('123 + 27') === 150
    assert calculate('123 + 27 / 3') === 132.0
  end
end
